<?php

/**
 * @file
 * Default theme implementation for rendering Code Editor Form.
 *
 * This template is used only in rc_editor page.
 *
 * Available variables:
 * - $original_code: A string containing the original code(if provided) to be displayed in textarea.
 * - $output : A string containing the actual output of the executed Code. 
 *
 */
?>
<form action="<?php echo url('rc_editor'); ?>" method="post">
	<label>Write Code:</label>
	&lt;?php
	<textarea name="php_code"><?php print $original_code; ?></textarea>
	?&gt;<br>
	<input type="submit" value="Run Code" />
</form>

<div class="output_container">
	<h3>Output: </h3>
	<div class="output"><?php print $output; ?></div>
</div>